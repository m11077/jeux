import pygame

from pygame import *

largeur_ecran= 800
hauteur_ecran = 600

pygame.init()

pygame.display.set_caption("The Shoot'em up 1.0")

ecran = pygame.display.set_mode([largeur_ecran, hauteur_ecran])


vaisseau = Vaisseau()


continuer = True
while continuer:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            continuer = False
        ecran.fill((0, 0, 0))
        
        ecran.blit(vaisseau.surf, vaisseau.rect)
        pygame.display.flip()
        
        
pygame.quit()
    
    